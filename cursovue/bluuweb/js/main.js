new Vue ({
    el: '#app',
    data:{
      titulo:"Insertar datos en la lista",    
      array:['cristian','valentina','carlos'],
      objeto:[
          
        {nombre:'pera',cantidad:10},
        {nombre:'manzana',cantidad:0},
        {nombre:'platano',cantidad:10},
        {nombre:'uva',cantidad:10}
    ],
     agregar:'',
     total:0
  },
  // segundo tutorial
    methods:{
           agregarFrutas(){
              this.objeto.push({            
             nombre:this.agregar, cantidad:0
         });
         this.agregar=''
        }
    },
  // tercer tutorial
    computed:{
           sumatotal(){
                 this.total=0;
                 for(objetos of this.objeto){
                          this.total=this.total + objetos.cantidad;
                 }
                      return this.total;
           }
    }
});